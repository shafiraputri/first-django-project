from django.forms import ModelForm
from django import forms

from .models import MataKuliah

class MatkulForm(ModelForm):
    """ nama_matkul = forms.CharField(label='Nama mata kuliah', max_length=50)
    jumlah_sks = forms.IntegerField(label='Jumlah SKS')
    deskripsi = forms.CharField(label='Deskripsi', max_length=100)
    semester = forms.CharField(label='Semester', max_length=15)
    ruang_kelas = forms.CharField(label='Ruang kelas', max_length=20) """
    class Meta:
        model = MataKuliah
        fields = '__all__'
        
        labels = {
            'nama_matkul': 'Nama mata kuliah', 
            'dosen_pengajar': 'Dosen pengajar',
            'jumlah_sks': 'Jumlah SKS', 
            'deskripsi': 'Deskripsi', 
            'semester': 'Semester', 
            'ruang_kelas': 'Ruang Kelas'
        }

        widgets = {
            'nama_matkul': forms.TextInput({'class':'form-control'}),
            'dosen_pengajar': forms.TextInput({'class':'form-control'}),
            'jumlah_sks': forms.NumberInput({'class':'form-control'}), 
            'deskripsi': forms.TextInput({'class':'form-control'}), 
            'semester': forms.TextInput({'class':'form-control'}), 
            'ruang_kelas': forms.TextInput({'class':'form-control'})
        }