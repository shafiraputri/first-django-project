# Generated by Django 3.1.2 on 2020-10-17 11:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20201017_1830'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matakuliah',
            name='deskripsi',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='matakuliah',
            name='dosen_pengajar',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='matakuliah',
            name='jumlah_sks',
            field=models.IntegerField(default=''),
        ),
        migrations.AlterField(
            model_name='matakuliah',
            name='nama_matkul',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='matakuliah',
            name='ruang_kelas',
            field=models.CharField(default='', max_length=25),
        ),
        migrations.AlterField(
            model_name='matakuliah',
            name='semester',
            field=models.CharField(default='', max_length=20),
        ),
    ]
