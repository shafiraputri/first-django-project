from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    nama_matkul = models.CharField(max_length=50, default='')
    dosen_pengajar = models.CharField(max_length=50, default='')
    jumlah_sks = models.IntegerField(default='')
    deskripsi = models.TextField(default='')
    semester = models.CharField(max_length=20, default='')
    ruang_kelas = models.CharField(max_length=25, default='')

    def __str__(self):
        return self.nama_matkul
