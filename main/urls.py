from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('aboutMe/', views.aboutMe, name="aboutMe"),
    path('myJourney/', views.myJourney, name="myJourney"),
    path('contact/', views.contact, name="contact"),
    path('myHobbies/', views.myHobbies, name="myHobbies"),
    path('viewMataKuliah/', views.viewMataKuliah, name="viewMataKuliah"),
    path('detailMataKuliah/<int:pk>/', views.detailMatkul, name="detailMatkul"),
    path('removeMataKuliah/', views.removeMataKuliah, name="removeMataKuliah"),
    path('removeMataKuliah/<int:pk>/', views.removeMatkul, name="removeMatkul"),
    path('addMataKuliah/', views.addMataKuliah, name="addMataKuliah"),
]
