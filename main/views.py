from django.shortcuts import render, HttpResponseRedirect

from .models import MataKuliah
from .forms import MatkulForm

def home(request):
    return render(request, 'main/home.html')

def aboutMe(request):
    return render(request, 'main/aboutMe.html')

def myJourney(request):
    return render(request, 'main/myJourney.html')

def contact(request):
    return render(request, 'main/contact.html')

def myHobbies(request):
    return render(request, 'main/myHobbies.html')

def viewMataKuliah(request):
    konteks = {
        'daftarMataKuliah' : MataKuliah.objects.all()
        }
    html = 'main/viewMataKuliah.html'
    return render(request, html, konteks)

def detailMatkul(request, pk):
    matkul = MataKuliah.objects.get(id = pk)
    konteks = {
        'mataKuliah' : matkul
        }
    html = 'main/detailMataKuliah.html'
    return render(request, html, konteks)

def addMataKuliah(request):
    if request.method == 'POST':
        form = MatkulForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/viewMataKuliah/')
    else:
        form = MatkulForm()

    return render(request, 'main/addMataKuliah.html', {'form': form})

def removeMataKuliah(request):
    konteks = {
        'daftarMataKuliah' : MataKuliah.objects.all()
        }
    html = 'main/removeMataKuliah.html'
    return render(request, html, konteks)

def removeMatkul(request, pk):
    matkul = MataKuliah.objects.filter(id = pk)
    matkul.delete()

    return HttpResponseRedirect('/viewMataKuliah/')