# Generated by Django 3.1.2 on 2020-10-23 03:57

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Peserta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(default='', max_length=50)),
                ('umur', models.IntegerField(default='')),
            ],
        ),
        migrations.CreateModel(
            name='Kegiatan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_kegiatan', models.CharField(default='', max_length=50)),
                ('tanggal', models.DateField(auto_now_add=True)),
                ('peserta', models.ManyToManyField(null=True, to='story1.Peserta')),
            ],
        ),
    ]
