from django.contrib import admin
from .models import Peserta, Kegiatan

# Register your models here.
admin.site.register(Peserta)
admin.site.register(Kegiatan)