from django.forms import ModelForm
from django import forms

from .models import Kegiatan, Peserta

class PesertaForm(ModelForm):
    class Meta:
        model = Peserta
        fields = '__all__'
        exclude = ['kegiatan']
        
        labels = {
            'nama': 'Nama Peserta', 
            'umur': 'Umur'
        }

        widgets = {
            'nama': forms.TextInput({'class':'form-control'}),
            'umur': forms.NumberInput({'class':'form-control'})
        }

class KegiatanForm(ModelForm):
    class Meta:
        model = Kegiatan
        exclude = ['peserta']
        
        labels = {
            'nama_kegiatan': 'Nama Kegiatan'
        }

        widgets = {
            'nama_kegiatan': forms.TextInput({'class':'form-control'})
        }

    