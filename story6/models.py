from django.db import models

# Create your models here.
class Peserta(models.Model):
    nama = models.CharField(max_length=50, default='')
    umur = models.IntegerField(default='')

    def __str__(self):
        return self.nama

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=50, default='')

    def __str__(self):
        return self.nama_kegiatan