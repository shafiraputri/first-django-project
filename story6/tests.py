from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from .views import viewKegiatan, addKegiatan, addPeserta
from .models import Kegiatan, Peserta
from .forms import PesertaForm, KegiatanForm

class Story6Test(TestCase):
    """ def test_story6_url_is_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200) """

    def test_addKegiatan_url_is_exist(self):
        response = Client().get('/story6/addKegiatan/')
        self.assertEqual(response.status_code, 200)

    """ def test_addPeserta_url_is_exist(self):
        response = Client().get('/story6/addPeserta/')
        self.assertEqual(response.status_code, 200) """

    """ def test_story6_using_viewKegiatan_template(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'story6/viewKegiatan.html') """

    def test_addKegiatan_using_addKegiatan_template(self):
        response = Client().get('/story6/addKegiatan/')
        self.assertTemplateUsed(response, 'story6/addKegiatan.html')

    """ def test_addPeserta_using_addPeserta_template(self):
        response = Client().get('/story6/addPeserta/')
        self.assertTemplateUsed(response, 'story6/addPeserta.html') """

    def test_story6_using_viewKegiatan_func(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, viewKegiatan)

    def test_addKegiatan_using_addKegiatan_func(self):
        found = resolve('/story6/addKegiatan/')
        self.assertEqual(found.func, addKegiatan)

    """ def test_addPeserta_using_addPeserta_func(self):
        found = resolve('/story6/addPeserta/')
        self.assertEqual(found.func, addPeserta) """

    def test_model_can_create_new_peserta(self):
        #Membuat peserta baru
        peserta_baru = Peserta.objects.create(nama='Nama Saya', umur='20')

        #Mendapatkan jumlah peserta yang ada
        counting_all_available_peserta = Peserta.objects.all().count()
        self.assertEqual(counting_all_available_peserta, 1)

    def test_model_can_create_new_kegiatan(self):
        #Membuat kegiatan baru
        kegiatan_baru = Kegiatan.objects.create(nama_kegiatan='Kegiatan ini')

        #Mendapatkan jumlah kegiatan yang ada
        counting_all_available_kegiatan = Kegiatan.objects.all().count()
        self.assertEqual(counting_all_available_kegiatan,1)

    """ def test_template_contain_kegiatan(self):
        kegiatan_baru = Kegiatan.objects.create(nama_kegiatan='Kegiatan 1')
        konteks = {
            'daftarKegiatan' : Kegiatan.objects.all()
        }
        response = Client().get('/story6/')
        html_response = response.content.decode('utf8')
        self.assertIn('Kegiatan 1', html_response) """
    
    def test_PesertaForm_valid_data(self):
        form = PesertaForm(data ={
            'nama' : 'Shafira',
            'umur' : 18
        })
        self.assertTrue(form.is_valid())

    def test_PesertaForm_no_data(self):
        form = PesertaForm(data ={})
        self.assertFalse(form.is_valid())

    def test_KegiatanForm_valid_data(self):
        form = KegiatanForm(data ={
            'nama_kegiatan' : 'Kegiatan ini',
        })
        self.assertTrue(form.is_valid())

    def test_KegiatanForm_no_data(self):
        form = KegiatanForm(data ={})
        self.assertFalse(form.is_valid())
    
    def test_addKegiatan_POST(self):
        response = Client().post('/story6/addKegiatan/', {
            'nama_kegiatan' : 'Kegiatan xx',
        })
        self.assertEqual(response.status_code, 302)

    """ def test_addPeserta_POST(self):
        response = Client().post('/story6/addPeserta/', {
            'nama' : 'Peserta xx',
            'umur' : 25
        })
        self.assertEqual(response.status_code, 302) """

    