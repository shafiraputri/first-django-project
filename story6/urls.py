from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.viewKegiatan, name='viewKegiatan'),
    path('addKegiatan/', views.addKegiatan, name='addKegiatan'),
    # path('addPeserta/<int:pk>/', views.addPeserta, name='addPeserta'),
]
