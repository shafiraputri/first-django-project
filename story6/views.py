from django.shortcuts import render, HttpResponseRedirect
from .models import Peserta, Kegiatan
from .forms import PesertaForm, KegiatanForm

def viewKegiatan(request):
    konteks = {
        'daftarKegiatan' : Kegiatan.objects.all()
    }
    return render(request, 'story6/viewKegiatan.html', konteks)

def addKegiatan(request):
    konteks = {
            'form': KegiatanForm()
        }
    if request.method == 'POST':
        form = KegiatanForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/story6/')
    return render(request, 'story6/addKegiatan.html', konteks)

def addPeserta(request, pk):
    konteks = {
            'form': PesertaForm()
        }
    if request.method == 'POST':
        form = PesertaForm(request.POST)
        if form.is_valid():
            kegiatan = Kegiatan.objects.get(id=pk)
            Peserta.objects.create(nama=request.POST['nama'], kegiatan=kegiatan)
            return HttpResponseRedirect('/story6/')
    return render(request, 'story6/addPeserta.html', konteks)