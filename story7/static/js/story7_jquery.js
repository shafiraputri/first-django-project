var icons = {
    header: "ui-icon-caret-1-e",
    activeHeader: " ui-icon-caret-1-s"
  };

$( "#accordion" ).accordion({
    collapsible: true,
    icons: icons
  });

$('.up').on('click', function(e) {
    var wrapper = $(this).closest('div.section')
    wrapper.next.insertBefore(wrapper.prev().prev())
    wrapper.insertBefore(wrapper.prev().prev())
})

$('.down').on('click', function(e) {
    var wrapper = $(this).closest('div.section')
    wrapper.next.insertAfter(wrapper.next().next())
    wrapper.insertAfter(wrapper.next().next())
})
