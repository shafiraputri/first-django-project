from django.test import TestCase, Client
from django.urls import resolve

from .views import profile

class Story7Test(TestCase):

    # Test URL
    def test_profile_url_is_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    #Test HTML Template
    def test_profile_using_profile_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7/profile.html')

    def test_profile_template_is_complete(self):
        response = Client().get('/story7/')
        html_response = response.content.decode('utf8')
        self.assertIn('My Profile', html_response)
        
    #Test View
    def test_profile_using_profile_func(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, profile)
