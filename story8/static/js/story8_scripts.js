$("#keyword").keyup(function() {
    var keyword = $("#keyword").val();
    var url = 'keyword/?q=' + keyword
    $.ajax({
        url: url,
        success: function(hasil) {
            var hasilKeyword = $('#hasil');
            hasilKeyword.empty();
            hasilKeyword.append('<tr>' + '<th>Judul Buku</th>' + '<th>Penulis</th>' + 
                '<th>Penerbit</th>' + '<th>Tanggal Terbit</th>' + '</tr>');

            for (i = 0; i < hasil.items.length; i++) {
                var tmp_book_info = hasil.items[i].volumeInfo;
                var tmp_title = tmp_book_info.title;
                var tmp_authors = tmp_book_info.authors;
                var tmp_publisher = tmp_book_info.publisher;
                var tmp_publishedDate = tmp_book_info.publishedDate;
                if (tmp_authors == undefined) {
                    tmp_authors = '-'
                }
                if (tmp_publisher == undefined) {
                    tmp_publisher = '-'
                }
                if (tmp_publishedDate == undefined) {
                    tmp_publishedDate = '-'
                }
                hasilKeyword.append('<tr><td>' + tmp_title + '</td>' 
                    + '<td>' + tmp_authors + '</td>'
                    + '<td>' + tmp_publisher + '</td>'
                    + '<td>' + tmp_publishedDate + '</td></tr>');
            }
        }
    })
});