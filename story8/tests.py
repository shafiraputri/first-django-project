from django.test import TestCase, Client
from django.urls import resolve
import requests

from .views import search,keyword

class Story8Test(TestCase):

    # Test URL
    def test_search_url_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    #Test HTML Template
    def test_search_using_search_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8/search.html')

    def test_search_template_is_complete(self):
        response = Client().get('/story8/')
        html_response = response.content.decode('utf8')
        self.assertIn('Search Book', html_response)
        self.assertIn('input', html_response)
        
    #Test View
    def test_search_using_search_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, search)

    def test_keyword_using_keyword_func(self):
        found = resolve('/story8/keyword/')
        self.assertEqual(found.func, keyword)

    #Test JSON File in View
    def test_keyword_func_with_query(self):
        query = "buku"
        response = Client().get('/story8/keyword/?q=' + query)
        self.assertEqual(response.status_code, 200)
       
