from django.urls import path

from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.search, name='search'),
    path('keyword/', views.keyword, name='keyword')
]
