from django.shortcuts import render
from django.http import JsonResponse
import requests, json

# Create your views here.
def search(request):
    responds = {
    }
    return render(request, 'story8/search.html', responds)

def keyword(request):
    arg = request.GET['q']
    url_api = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    hasil = requests.get(url_api)
    data = json.loads(hasil.content)
    return JsonResponse(data, safe=False)