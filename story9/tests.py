from django.test import TestCase, Client
from django.urls import resolve

from django.contrib.auth.models import User
from .views import index, signUp, logIn,logOut

class Story9Test(TestCase):

    def test_index_url_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_logIn_url_is_exist(self):
        response = Client().get('/story9/log-in/')
        self.assertEqual(response.status_code, 200)

    def test_signUp_url_is_exist(self):
        response = Client().get('/story9/sign-up/')
        self.assertEqual(response.status_code, 200)

    def test_logOut_url_will_be_redirect(self):
        response = Client().get('/story9/log-out/')
        self.assertEqual(response.status_code, 302)

    #Test HTML Template
    def test_index_using_index_template(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'story9/index.html')

    def test_index_template_is_complete(self):
        response = Client().get('/story9/')
        html_response = response.content.decode('utf8')
        self.assertIn('Halo', html_response)

    def test_signUp_using_signUp_template(self):
        response = Client().get('/story9/sign-up/')
        self.assertTemplateUsed(response, 'story9/signUp.html')

    def test_signUp_template_is_complete(self):
        response = Client().get('/story9/sign-up/')
        html_response = response.content.decode('utf8')
        self.assertIn('Sign Up', html_response)

    def test_logIn_using_logIn_template(self):
        response = Client().get('/story9/log-in/')
        self.assertTemplateUsed(response, 'story9/logIn.html')

    def test_logIn_template_is_complete(self):
        response = Client().get('/story9/log-in/')
        html_response = response.content.decode('utf8')
        self.assertIn('Log In', html_response)
        
    #Test View
    def test_index_using_index_func(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, index)
    
    def test_signUp_using_signUp_func(self):
        found = resolve('/story9/sign-up/')
        self.assertEqual(found.func, signUp)

    def test_logIn_using_logIn_func(self):
        found = resolve('/story9/log-in/')
        self.assertEqual(found.func, logIn)
    
    def test_logOut_using_logOut_func(self):
        found = resolve('/story9/log-out/')
        self.assertEqual(found.func, logOut)

    #Test Login and Signup form
    def test_singUp_form_with_valid_data(self):
        response = Client().post('/story9/sign-up/', {
            'username': 'user',
            'email' : 'user@gmail.com', 
            'password1': '1234resu',
            'password2': '1234resu'
            })
        self.assertEqual(response.status_code, 302)
    
    def test_singUp_form_with_invalid_data(self):
        response = Client().post('/story9/sign-up/', {
            'username': 'user',
            'email' : 'user@gmail.com', 
            'password1': '1234resu',
            'password2': '12345678'
            })
        self.assertEqual(response.status_code, 200)
    
    def test_logIn_form_with_valid_data(self):
        user = User.objects.create_user(username='testuser', email='user@gmail.com', password='user1234')
        response = Client().post('/story9/log-in/', {
            'username': 'testuser',
            'password': 'user1234',
            })
        self.assertEqual(response.status_code, 302)

    def test_logIn_form_with_invalid_data(self):
        user = User.objects.create_user(username='testuser', email='user@gmail.com', password='user1234')
        response = Client().post('/story9/log-in/', {
            'username': 'testuser',
            'password': 'user',
            })
        self.assertEqual(response.status_code, 200)