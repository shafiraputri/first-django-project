from django.urls import path

from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.index, name='index'),
    path('sign-up/', views.signUp, name='signUp'),
    path('log-in/', views.logIn, name='logIn'),
    path('log-out/', views.logOut, name='logOut'),

]
