from django.shortcuts import render, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.http import JsonResponse
from .forms import SignUpForm

import requests, json


# Create your views here.
def index(request):
    responds = {}
    return render(request, 'story9/index.html', responds)

def signUp(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/story9/')

    signUpForm = SignUpForm()
    if request.method == "POST":
        signUpForm = SignUpForm(request.POST)
        if signUpForm.is_valid() :
            signUpForm.save()
            messages.success(request, "Akun berhasil dibuat. Silahkan login untuk masuk ke akunmu.")
            return HttpResponseRedirect('/story9/log-in')

    responds = {'form' : signUpForm}
    return render(request, 'story9/signUp.html', responds)

def logIn(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/story9/') 

    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect('/story9/')
        else:
            messages.info(request, "Username atau password salah")

    responds = {}
    return render(request, 'story9/logIn.html', responds)

def logOut(request):
    logout(request)
    return HttpResponseRedirect('/story9/')